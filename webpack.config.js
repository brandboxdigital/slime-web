const webpack = require('webpack')
const path = require('path')

const HtmlWebpackPlugin = require('html-webpack-plugin')
const CopyPlugin = require('copy-webpack-plugin');

module.exports = {
  mode: 'development',
  entry: path.join(__dirname, 'src', 'index'),
  // watch: true,
  output: {
    path: path.join(__dirname, 'out'),
    publicPath: '/',
    filename: "bundle.js",
    chunkFilename: '[name].js'
  },
  plugins: [
		new HtmlWebpackPlugin({
      sizing: 'none',
      template: path.resolve(__dirname, './src/index.html'),
    }),

		new HtmlWebpackPlugin({
      sizing: '700x480',
      template: path.resolve(__dirname, './src/index.html'),
      filename: '700x480/index.html',
    }),
		new HtmlWebpackPlugin({
      sizing: '1060x470',
      template: path.resolve(__dirname, './src/index.html'),
      filename: '1060x470/index.html',
    }),
		new HtmlWebpackPlugin({
      sizing: '600x800',
      template: path.resolve(__dirname, './src/index.html'),
      filename: '600x800/index.html',
    }),

    // new HtmlWebpackPlugin({  // Also generate a test.html
    //   filename: 'test.html',
    //   template: 'src/assets/test.html'
    // }),

    new CopyPlugin({
      patterns: [
        { from: "./assets/600x800.png", to: "600x800/assets" },
        { from: "./assets/*.svg", to: "600x800" },
        { from: "./out/bundle.js", to: "600x800", noErrorOnMissing: true },

        { from: "./assets/700x480.png", to: "700x480/assets" },
        { from: "./assets/*.svg", to: "700x480" },
        { from: "./out/bundle.js", to: "700x480", noErrorOnMissing: true },

        { from: "./assets/1060x470.png", to: "1060x470/assets" },
        { from: "./assets/*.svg", to: "1060x470" },
        { from: "./out/bundle.js", to: "1060x470", noErrorOnMissing: true },

        { from: "./assets", to: "assets" },
      ],
    })
	],
  module: {
    rules: [
      {
        test: /.jsx?$/,
        include: [
          path.resolve(__dirname, 'src')
        ],
        exclude: [
          path.resolve(__dirname, 'node_modules')
        ],
        loader: 'babel-loader',
        query: {
          presets: [
            ["@babel/env", {
              "targets": {
                "browsers": "last 2 chrome versions"
              }
            }]
          ]
        }
      },
      {
				test: /\.s?css$/,
        use: [
          { loader: 'style-loader' },
          { loader: 'css-loader'},
          { loader: 'sass-loader' },
        ],
			},
      {
        test: /\.(png|svg|jpe?g|gif)$/,
        include: path.resolve(__dirname, 'assets'),
        use: [
          {
            loader: 'file-loader',
            options: {
              name: '[name].[ext]',
              outputPath: 'assets/',
              publicPath: 'assets/'
            }
          }
        ]
      },
    ]
  },
  resolve: {
    extensions: ['.json', '.js', '.jsx', '.scss', '.html', '.svg', '.png']
  },
  devtool: 'source-map',
};
